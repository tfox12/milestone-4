<!-- 
this is a partial page
purpose is to show a search form under the top menu
even though the file extension is .php, all of the code on this page is html
 -->
<html>
<head>
<link rel="stylesheet" type= "text/css" href="myWelcomeDesign.css">
</head>
<body>

<div class="row">
<div class="leftcolumn">
<div class="card">

<h2>SEARCH FOR DEVOTION</h2>
<h5>(Fill out all fields)</h5>

	<div class="fakeimg" style="height:200px;">
		<form action="searchDevotion.php">
			<textarea rows="1" cols="50" placeholder="Devotion Topic" name="devotionTopic"></textarea><br>
			<textarea rows="5" cols="50" placeholder="Devotion Title" name="devotionTitle"></textarea><br>
			<textarea rows="5" cols="50" placeholder="Devotion Body" name="devotionBody"></textarea><br>
			<button type="submit">Search</button>
		</form>
	</div>
</div>
</div>
</div>
</body>
</html>