<?php 
require_once 'showTopMenu.php';
$id = $_GET['id'];

//we know id
// select the rest of the values from the database

require_once 'db_connector.php';

if($connection) {
    $sql_statement = "SELECT * FROM `devotion_table` WHERE `id` = $id LIMIT 1";
    $result = mysqli_query($connection, $sql_statement);
    if ($result) {
        while ($row = mysqli_fetch_assoc($result)) {
            $devotion_topic = $row['devotion_topic'];
            $devotion_title = $row['devotion_title'];
            $devotion_body = $row['devotion_body'];
        }
    } else {
        echo "there was an sql problem " . mysqli_error($connection);
    }
}
else {
    echo "error connecting " . mysqli_connect_error();
}
?>

<div class="fakeimg" style="height:200px;">
<h2>Edit Devotion <?php echo "With id #" . $id;?></h2>
<h5>(Fill out all fields)</h5>
<form action="processEditItem.php">
<input type= "hidden" name= "id" value= "<?php echo $id;?>"></input>
<textarea rows="1" cols="50" name="devotionTopic" placeholder="Devotion Topic"><?php echo $devotion_topic?></textarea>
<textarea rows="5" cols="50" name="devotionTitle" placeholder="Devotion Title"><?php echo $devotion_title?></textarea>
<textarea rows="5" cols="50" name="devotionBody" placeholder="Devotion Body"><?php echo $devotion_body?></textarea>
<button type="submit">Submit Changes</button>
</form>
</div>